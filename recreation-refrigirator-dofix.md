Recreation checklist for group Refrigirator
===================================================

Group name: Dofix

Checklist
------------

- [ ] Project plan completed
- [ ] Minimal circuit schematic completed
- [x] ATmega328 minimal system code with tempsensor board working
- [x] ATMega328 to Rasperry Pi serial communication over UART working
- [ ] Raspberry pi configuration week06 requirements fulfilled
- [ ] Networked raspberry pi+atmega system behind a firewall accessible using SSH
- [ ] Temperature readout in celcius from the temperature sensor
- [ ] Raspberry API Server is able to serve temperature readings
- [ ] Virtual server installed
- [ ] `group.md` complete according to specification
- [ ] Dashbaord communicating with API

Comments
-----------

Lack of documentation, lack of files (code or configs or pictures), issues following template.  
Also, groupname is refrigirator but changed to refrigerator??