# ITT2_project_dofix


<h2>Executive Overview</h2>
The aim of the project is to create a product for a customer. The student group will be responsible for the implementation of the product, as well as the final documentation and the project plan.  


<h2>What to do and when?</h2>
We will start the project with creating a project plan and reading up on information, as well as discussing assigned tasks like who when and why.


<h2>Goals</h2>
The goal is to get a better understanding of making a complete product from scratch for a user group and manage the project through GitLab, as well as creating documentation of the product aimed at the stakeholders.

<h2>Communications</h2>
The way that we aim to communicate is in class, as well as Slack, Facebook and e-mail.  
  
Slack link: https://ittek.slack.com/messages/GFQJY8X2Q/team/  
E-mails in below in Team list.

<h2>Documentation</h2>
The documentations include the folder "Documents" that has "project_plan.md" with a more detailed plan on the project. The folder "Exercises" contains exercises we have been assigned by the lecturers, and "Weekly plans" includes weekly plans from the lecturers. "Guides" contain guides on recreating the project results.


<h2>Team list</h2>
<table>
    <tr>
        <th>GitLab username</th>
        <th>Name</th>
        <th>E-mail</th>
    </tr>
    <tr>
        <td>jambove</td>
        <td>Levente Atilla Taner</td>
        <td><a href="mailto:leve0147@edu.eal.dk">leve0147@edu.eal.dk</a></td>
    </tr>
    <tr>
        <td>ak98</td>
        <td>Andrej Královič</td>
        <td><a href="mailto:andr14b7@edu.eal.dk">andr14b7@edu.eal.dk</a></td>
    </tr>
    <tr>
        <td>nick3359</td>
        <td>Nicklas Tange Præstegaard</td>
        <td><a href="mailto:nick3359@edu.eal.dk">nick3359@edu.eal.dk</a></td>
</table>
        
