Group: Dofix
-----------------

Members:

* Andrej Královič ak98
* Levente Atilla Taner jambove
* Nicklas Tange Præstegaard nicklastange

Juniper router: 

* Management SSH: 10.217.19.212:22
* External ip: 10.217.19.212/22

Raspberry 

* SSH access: 10.217.19.212:1100
* REST API access: [http://10.217.19.212:5000](http://10.217.19.212:5000)

Group web server: 

* SSH access: 10.217.16.92:22
* REST API access: [http://10.217.16.92:5000](http://10.217.16.92:5000)
