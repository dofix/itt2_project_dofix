## Exercise 2 - ATmega328 minimal system code

### Information

In this exercise you will work with C code on the ATmega328 and connect the board build from week 05 to it.

You will also work with understanding and documenting C code.

As none of you is experienced in embedded C we've made a sample program to use in your minimum system. The code has comments and explanations about what it does.

### Exercise instructions

1. Clone code from gitlab.com/atmega328-sensor-sample/example-c-code. The repository has 2 branches. Chose the master branch if you are using the Xplained mini development board. Chose the arduino branch if you are using the arduino development board.
2. Read about flowcharts
    * www.lucidchart.com/pages/what-is-a-flowchart-tutorial
3. Make a flowchart of the code. Applications you can use includes:

    * www.yworks.com/products/yed | www.draw.io | www.lucidchart.com
4. Connect the sensor board build from week 05
    * Read in the code which GPIO's to use, the example uses PORTB and one of the analog inputs defined with the variables **btn1**, **led1** and **adcinput**.
    * The [ATmega328 datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf "ATmega328 datasheet link") page 12 has pin names and numbers for reference.
5. Examine which commands you can send via UART
6. Download the code to your development board
    * If you are using Arduino follow the guide "Arduino Uno Atmel Studio guide" (you can find it on itslearning).
    * If you are using the xPlained board follow this [guide](https://www.microchip.com/webdoc/atmega328pxmini/index.html "xPlained mini guide")
8. Use a terminal program ie. Putty to connect to the ATMega 328 via UART serial connection. The serial connection between the board and putty on your computer is established through the USB port regardless of the development board you are using.
    * Read about putty [here](https://store.chipkin.com/articles/using-putty-for-serial-com-connections-hyperterminal-replacement "Putty guide")
    * The serial connection details is in the code comments
9. Send commands to the ATmega328. Verify and document the response.
10. Make a flowchart of the code you are going to use in your project.
