## Exercise 4 - Raspberry, Atmega, and Python  

1. Fork the repository with the sample code (https://gitlab.com/atmega328-sensor-sample/python-atmega)  
2. Make sure the tests work, both locally and on gitlab. See *test/readme.md* for details.  
3. Update the *simple_connection.py* to enable connection to the atmega  
4. *(optional for now)* use argparse (https://docs.python.org/3/library/argparse.html) to be able to supply the serial command line.  
5. Update the test program:  
-Update the ping-pong system to handle reading the temperature and buttons so it matches the C program in atmega.  
6. Creat a new file *atmega.py*  
Implement the following functions:  
-read_LED(): LED read status  
-read_temp(): read the temperature ADC value from sensor  
-read_buttion():read button status  
  
Make program blink the LED. Remember the if __name__ .... guard  
7.Create a simple menu program to test the atmega  
Create a simple console menu program. Like this one: (https://extr3metech.wordpress.com/2014/09/14/simple-text-menu-in-python/)  
Each number will reresent a function. like *turn diode on*,*read button* and so on.   
You probably want something like *from atmega import* * at te top of the file.  
8. Create a test program that tests the functions of *atmega.py* and put it into gitlab for continual testing.  