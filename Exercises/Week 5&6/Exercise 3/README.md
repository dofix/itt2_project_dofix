## Exercise 3 - Connect ATmega328 and Raspberry Pi

### information

In this exercise you will connect the ATmega328 serial (UART) pins with the RPi serial pins via a level shifter.

_BFW (big fat warning)_: Using 5V for GPIO on raspberry will probably fry your port. Remember to use a level shifter.

### Exercise instructions

The ATMEga will with the supplied example program have UART enabled and functioning. TX and RX pin numbers can be read at page 12 in the ATmega328 datasheet: The [ATmega328 datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf "ATmega328 datasheet link")
They are called RXD and TXD.

On raspberry 3: TX is GPIO14 and RX is GPIO15. See [here](https://elinux.org/RPi_Low-level_peripherals) for more details

#### Connect ATmega328 and raspberry via levelshifter

The ATmega328 GPIO's is 0-5V levels and the raspberry is 0-3.3V levels. The raspberry pins is not 5V tolerant and can suffer damage if they are connected to a 5V pin.
To accomplish connecting the ATmega328 and raspberry we will provide you with a bidirectional level shifter that can convert from one voltage to another.

Read this document to learn more about the [level shifter](https://www.raspberrypi-spy.co.uk/2018/09/using-a-level-shifter-with-the-raspberry-pi-gpio/)

The level shifter you are going to use is this [one](https://www.trab.dk/da/breakout/398-8-kanal-i2c-two-way-bidi-logic-level-converter.html "level converter link")

Connect it like in the below diagram, do not supply any power before everything is connected:

[Link to diagram](https://gitlab.com/atmega328-sensor-sample/schematics-orcad/blob/master/328_lvlshift_RPi.png "ATmega328 to levelshifter to Raspberry Pi connections")

To have the raspberry and the ATMega board talking to each, the raspberry serial port must be set up, and parameters must the same in both ends. We use 9600 8N1

#### Set up raspberry

using configurator:
1. raspi-config
2. enable serial and disable console access

manual:
> enable_uart=1 at the end of /boot/config.txt

If you changed stuff above, you need to reboot.

In the logs you have something like this (this is from a raspberry pi 3)

```
$ dmesg | grep tty
[    0.000000] Kernel command line: 8250.nr_uarts=0 bcm2708_fb.fbwidth=1920 bcm2708_fb.fbheight=1080 bcm2708_fb.fbswap=1 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  dwc_otg.lpm_enable=0 console=ttyS0,115200 console=tty1 root=PARTUUID=f8cf455f-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait quiet splash plymouth.ignore-serial-consoles
[    0.000284] console [tty1] enabled
[    0.744384] 3f201000.serial: ttyAMA0 at MMIO 0x3f201000 (irq = 87, base_baud = 0) is a PL011 rev2
```

`tty1` is claimed by a console, `ttyAMA0` is not. This is as we want it.

We use minicom for the serial connection.
```
apt-get install minicom
```

The raspberry is now set up to use the GPIOs for UART.


#### Connecting

Start minicom
```
minicom -D /dev/ttyAMA0 -b 9600
```

At first you will not be able to see what you type yourself - this relates to "local echo".
In minicom it is "ctrl+A E" to toggle local echo, and then you should be able to see what you type. To exit "ctrl+A X"

Verify by sending some correct commands and some wrong commands.
