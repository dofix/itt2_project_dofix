#!/bin/sh

#chmod +x install.sh first
#for creating log file run as this:
#bash -x install.sh 2>&1 | tee installationlog.txt

echo ________________________________________
echo Starting upgrades and installs
apt-get update
apt-get upgrade -y
apt-get install virtualenv python git-core curl nginx -y
echo Upgrades and installs finished
echo ________________________________________

echo ________________________________________
echo Fetching SSH public keys from Gitlab
mkdir ~/.ssh
curl https://gitlab.com/jambove.keys >> ~/.ssh/authorized_keys  ##ADD YOUR OWN GITLAB USERS HERE
curl https://gitlab.com/ak98.keys >> ~/.ssh/authorized_keys     ##ADD YOUR OWN GITLAB USERS HERE
curl https://gitlab.com/nick3359.keys >> ~/.ssh/authorized_keys ##ADD YOUR OWN GITLAB USERS HERE
echo SSH keys set
sudo systemctl enable ssh
sudo systemctl start ssh
echo SSH enabled
echo ________________________________________

echo ________________________________________
echo Setting up nginx
sudo systemctl enable nginx
sudo systemctl start nginx
echo Nginx enabled and started
echo ________________________________________

echo ________________________________________
echo Control phase:
cat ~/.ssh/authorized_keys
systemctl status ssh
systemctl status nginx
echo ________________________________________

echo Installation has finished.
