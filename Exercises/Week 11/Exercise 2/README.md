## Exercise 2 - Datalogger housing

In week 11 and 12 you are going to fablab for workshops in laser cutting and 3D printing.
Before you do that we would like you to have a sketch for housing the Raspberry Pi + ATmega328 + tempsensor + led's.
The components should be in the same housing with exposed leds, temp sensor, connectors etc.

1. Create a sketch that shows how you want the finished housing to look like.
2. Save the sketch as housing.pdf in your gitlab project
3. Bring the sketch to the workshop, to be able to discuss with the fablab staff on how to build it the best way.

### Extra
1. Make a document that explains the value your housing design adds to the usability.
2. Test your housing idea by asking potential users about your design. Prepare 3-5 questions to ask. The more users you ask the more useful your design will be. 
3. Correct your design according to user response.

