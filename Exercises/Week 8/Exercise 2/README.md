## Exercise 2 - Temperature readout in celcius from the temperature sensor

The value you are getting from the temperature sensor, is at the moment the raw ADC value, which is a number between 0 and 1023.
The reason for this is that the ATmega328 ADC is 10 bits, which equals 1024 values. The voltage range is 0V - 5V.
To calculate the voltage for each step you can use this equation 5/1024 = 4.88mV.

You need to decide if you want to do the conversion between ADC value and temperature at the Raspberry Pi or the ATmega328.
Discuss this in the group and choose what is the best solution to your system with flexibility in mind.

1. Read about how to convert the ADC value to temperature [Using a TMP36](https://learn.adafruit.com/tmp36-temperature-sensor/using-a-temp-sensor), the article is written for the arduino so you need to convert this to a function in either C or Python.
2. Implement the function
3. Test the function, make sure it is consistent and represents real temperatures. 
4. Update your documentation to reflect the new functionality.

* Optional 1: Implement a function to readout temperature in Fahrenheit.
* Optional 2: Implement moving average readings of temperatures. Take as an example 10 readings and do an average to get a more stable readout, you probably want to do this in the ATmega328 to keep UART traffic low. [Moving average wikipedia](https://en.wikipedia.org/wiki/Moving_average)

* BONUS: The ATmega328 code has been updated. This fixes the constant error message after pressing the button and also limits the UART input to 30 characters. In other words it should be more stable. [Link to C code](https://gitlab.com/atmega328-sensor-sample/example-c-code)