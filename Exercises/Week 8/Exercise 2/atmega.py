#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import serial
import time
import os
serial_cfg = {  "dev": "/dev/ttyS0",
                "baud": 9600 }

print( "Running port {}".format( serial_cfg['dev']))

def print_menu():       ## Your menu design here
    print(30 * "-" + " MENU " + 30 * "-")
    print("1. led1on")
    print("2. led1off")
    print("3. getbtn1")
    print("4. getadcval")
    print("5. Exit")
    print(67 * "-")

def turnon_LED():
	print("led1on")
	ser.write( "led1on\n".encode() )


def turnoff_LED():
	print("led1off")
	ser.write( "led1off\n".encode() )

def read_temp():
	print("getadcval")
	ser.write( "getadcval\n".encode() )

def read_button():
	print("getbtn1")
	ser.write( "getbtn1\n".encode() )

def valInCelcius():
	reply = ser.readline().decode()
	adcVal = int(reply)
	milliVolts = adcVal * (float(5000)/1024)
	celcius = round(((milliVolts - 500)/10),2)
	print("- {}".format(celcius) + "°C")

def formatdecode():
	reply = ser.readline().decode() # read a '\n' terminated line
	print("- {}".format(reply))

def clearing():
	os.system('clear')

loop = True

while loop:          ## While loop which will keep going until loop = False
	print_menu()    ## Displays menu
	choice = input("Enter your choice [1-5]: ")
	if choice==1:
		try:
			with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
				turnon_LED()
				formatdecode()
			clearing()
		except KeyboardInterrupt:
			break
	elif choice==2:
		try:
			with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
				turnoff_LED()
				formatdecode()
			clearing()
		except KeyboardInterrupt:
			break
	elif choice==3:
		try:
			with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
				read_button()
				formatdecode()
			clearing()
		except KeyboardInterrupt:
			break
	elif choice==4:
		while True:
			try:
				with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
					clearing()
					read_temp()
					valInCelcius()
					#formatdecode()
					print("Press Ctrl+C to exit to menu")
					time.sleep(1)
			except KeyboardInterrupt:
				break
	elif choice==5:
		print("You've choosed to end the program.. Bye bye")
		exit()
		loop = False

		## You can add your code or functions here
		# This will make the while loop to end as not value of loop is set to False
	else:
		# Any integer inputs other than values 1-5 we print an error message
		input("Wrong option selection. Enter any key to try again..")
		os.system('clear')
