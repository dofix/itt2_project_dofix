## Exercise 1 - Setting up a RESTful API service

This resembled a lot what we did in december.

1. Start with the [REST API template](https://github.com/moozer/restex).
2. Decide on naming and such, and document usage
2. Create a new program heavily inspired from `restex.py`, that can return the current temperature reading using the module you have created.
3. Run it and test it with curl
5. Make it available through the router and test it
6. Make it start up automatically and test it

Optional: look into [postman](https://www.getpostman.com/) and/or [hithchiker-api](https://github.com/brookshi/Hitchhiker) and see if they are applicable to this.
