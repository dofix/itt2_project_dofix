#!/bin/bash
#source /home/pi/restex/venv/bin/activate

until ip a | grep 192.168.10.2 && dmesg | grep ttyS0
do
        sleep 2
done
python /home/pi/restex/restex.py &

## crontab -e : @reboot bash -x /home/pi/restexstartup.sh 2>&1 | tee /home/pi/whatswrong.log
