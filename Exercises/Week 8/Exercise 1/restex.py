from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
import serial
import time
import os

serial_cfg = {  "dev": "/dev/ttyS0",
                "baud": 9600 }
app = Flask(__name__)
CORS(app, resources=r'/*') # to allow external resources to fetch data
api = Api(app)

print( "Running serial on port {}".format( serial_cfg['dev']))

def valInCelsius( ser ):
	reply = ser.readline().decode()
	adcVal = int(reply)
	milliVolts = adcVal * (float(5000)/1024)
	celsius = round(((milliVolts - 500)/10),2)
	return celsius
	print("- {}".format(celsius) + "celsius")

def gettemp():
	while True:
		try:
			with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
				ser.write( "getadcval\n".encode() )
				changeval(ser)
				#valInCelsius()
				time.sleep(1)
		except KeyboardInterrupt:
			break
			
def changeval( ser ):
	gpios["T0"]["value"] = valInCelsius( ser )

	
gpios = {"A0": { 'value': 0},
         "A1": { 'value': 0},
         "T0": { 'value': 0}}


with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
	ser.write( "getadcval\n".encode() )
	changeval(ser)
	#valInCelsius()
	time.sleep(1)

@api.resource("/")
class url_index( Resource ):
	def get( self ):
		returnMessage = {"Message": "Yes, it works. Temperature is" }
		return returnMessage
	
@api.resource("/gpio")
class url_gpio( Resource ):
	def get( self ):
		returnMessage = gpios.keys()
		return returnMessage

@api.resource("/gpio/<gpio_name>")
class url_gpio_name( Resource ):
	def get( self, gpio_name ):
		#try:
		with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
			ser.write( "getadcval\n".encode() )
			changeval(ser)
			#valInCelsius()
			time.sleep(1)
		return gpios[ gpio_name ]
		#except KeyError, ex:
		#	return { "message": "key error '%s' is not a GPIO name"%(gpio_name,) }, 400

	def put( self, gpio_name ):
		try:
			gpios[ gpio_name ]['value'] = request.json['value']
			return "", 204
		except KeyError, ex:
			return { "message": "key error: Either '%s' is not a GPIO name or data is undefined (received: '%s')"%(gpio_name, str(request.form)) }, 400

if __name__ == '__main__':
	app.run(host="192.168.10.2", debug=True)

