## Exercise 2 - using local repo and generating stuff

1. git clone the repository to your local machine
2. Make a `projects.txt` file of projects you want to show on the front page
3. Create a python script that reads from the file and outputs html

    It is suggested that you make at least 3 parts.

    1. output header and whatever needs to be before the list
    2. output the list of projects
    3. output extra stuff and footer for whatever else is needed in the html file

4. test the the generated file works
5. add creation of `index.html` to `.gitlab-ci.yml`

