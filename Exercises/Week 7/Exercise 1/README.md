## Exercise 1 - setting up juniper routers

Requirements:
* Two groups will share a physical Juniper SRX240 in juniper lab.
* The SRX will contain a switch for each group, and each group will **not** have access to each others subnets.
* DHCP on internal LANs are allowed. The raspberry must have a static ip.
* The SRX will have a static ip supplied by MON.
* Usernames and SSH keys must be the same as used on gitlab.com. [This](https://kb.juniper.net/InfoCenter/index?page=content&id=KB16657&actp=METADATA) may be relevant.
* Access to the SRX will be by SSH **only** and **only** by using keys.
* `root` login must be disabled. This [link](https://www.juniper.net/documentation/en_US/junos/topics/reference/configuration-statement/root-login-edit-system.html) may be helpful.
* Traffic from the two internal LANs will be NAT'ed to the outside
* Set up a port forward from the outside to both internal raspberries. This will require you to have different ports for each raspberry
* JWeb must be disabled

You have resources on most of this from networking class.

Feel free to have extra stuff as long as the above i fulfilled.


The steps:

1. Find a group to share Juniper SRX router with

2. Create a gitlab project (and a group?)

    you need a place to put configuration, scripts and design files

3. Decide on how to manage the joint project

4. Create a high level design

    High level design. Go [here](https://eal-it-technology.github.io/Network-design/hld.html) for a guide.

    Ensure that the requirements are fulfilled.

5. Decide on and create documentation:

    Develop written procedures for how to update and who has access to the SRX

6. Create tasks and assign people

    This is a "second" project, so allocated man-power to both your original project and this "extra" juniper project.

