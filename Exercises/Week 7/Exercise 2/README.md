## Exercise 2 - Setup of the raspberry

Ensure that the raspberry fulfills the following requirements

* SSH is enabled and you connect using keys, not password

* The password for the `pi` user must long and annoying. You must not be able to remember it.

* The SSH keys you use for the raspberry must match those on gitlab.com

* No GUI must be installed. You will use `Raspbian Stretch Lite` as the base image (se [here](https://www.raspberrypi.org/downloads/raspbian/)). Yes, this will require a reinstallation for some of you.
* It must have a known static ip.

  This will help you should you connect using a direct ethernet cable.

* you must make a reinstallation script (w/o confidential information)

  This is difficult, and you will probably need help. Some hints: decide what is needed in order to reinstall the raspberry, make a list of the command you need to do it and then put it all in a script.

  How to handle login credentials?

* The reinstallation script must be publicly available with a description of how it is used. You can use gitlab CI and gitlab pages for this.

* You must have a documentated test script that shows that the atmega is functioning appropriately.

Some of the above may seem excesive now.... maybe there is an underlying plan somewhere.