## Exercise 1 - prepare product test

Product test is a compolsory learning activity and will be set up on wiseflow for hand in. 

The project should be gift wrapped meaning that you are able to present the minimal system both physically and online

On test day we will use the classroom and set up tables where you can host the hardware, datalogger in housing, dashboard and printed user manual.

You will host the test and you should define who does what on test day!

`A proper product test has well-defined, objectives, testers and expected outcomes.`

Hints on making a proper product test:

### Well defined objectives
- What is the purpose of the product test? (to test if the product performs as intended)
- What is to be tested? (product, usermanual)

### Well defined testers
- Who is the users? (target groups?, personas?)
- Pitch the product for users! (before users perform tests, you should explain the purpose of the product)
- Define a test plan to ensure uniformity between test (What tests are users going to perform?)

### Expected outcomes
- What do you want to achieve with the product test? (Confirm intended use? Get awareness of issues?)
- How are you going to document results? (How will you collect results on test day? How will you compare results?)
- How are you going to use the results? (correct them in the project?)